package com.example.likelionhospitalreivew.repository;

import com.example.likelionhospitalreivew.domain.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitRepository extends JpaRepository<Visit, Long> {

}
