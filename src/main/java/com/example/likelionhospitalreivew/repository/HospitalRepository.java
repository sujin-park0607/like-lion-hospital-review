package com.example.likelionhospitalreivew.repository;

import com.example.likelionhospitalreivew.domain.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Integer> {

}
