package com.example.likelionhospitalreivew.service;

import com.example.likelionhospitalreivew.domain.User;
import com.example.likelionhospitalreivew.domain.UserRole;
import com.example.likelionhospitalreivew.domain.dto.UserJoinRequest;
import com.example.likelionhospitalreivew.exception.AppException;
import com.example.likelionhospitalreivew.exception.ErrorCode;
import com.example.likelionhospitalreivew.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public String join(String userName, String password) {
        //username 중복체크
        userRepository.findByUserName(userName).ifPresent(
                user -> {
                    throw new AppException(ErrorCode.DUPLICATE_USER_NAME, String.format("%s가 중복입니다",userName));
                });

        //저장
        User user = UserJoinRequest.toEntity(userName,encoder.encode(password),UserRole.USER);
        userRepository.save(user);

        return "Success";
    }
}
