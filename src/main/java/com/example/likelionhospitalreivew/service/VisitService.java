package com.example.likelionhospitalreivew.service;

import com.example.likelionhospitalreivew.domain.Hospital;
import com.example.likelionhospitalreivew.domain.User;
import com.example.likelionhospitalreivew.domain.Visit;
import com.example.likelionhospitalreivew.domain.dto.VisitCreateRequest;
import com.example.likelionhospitalreivew.domain.dto.VisitResponse;
import com.example.likelionhospitalreivew.exception.AppException;
import com.example.likelionhospitalreivew.exception.ErrorCode;
import com.example.likelionhospitalreivew.repository.HospitalRepository;
import com.example.likelionhospitalreivew.repository.UserRepository;
import com.example.likelionhospitalreivew.repository.VisitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VisitService {

    private final VisitRepository visitRepository;
    private final HospitalRepository hospitalRepository;
    private final UserRepository userRepository;

    public void createVisit(VisitCreateRequest dto, String userName) {

        // hospital이 없을 때 등록불가
        Hospital hospital = hospitalRepository.findById(dto.getHospitalId())
                .orElseThrow(() -> new RuntimeException());

        // user가 없을 때 등록불가
        User user = userRepository.findByUserName(userName)
                .orElseThrow(RuntimeException::new);

        Visit visit = Visit.builder()
                .user(user)
                .hospital(hospital)
                .disease(dto.getDisease())
                .amount(dto.getAmount())
                .build();

        visitRepository.save(visit);
    }

    public List<VisitResponse> findAllByPage(Pageable pageable) {
        Page<Visit> visits = visitRepository.findAll(pageable);

        return visits.stream()
                .map(Visit::toResponse)
                .collect(Collectors.toList());
    }
}
