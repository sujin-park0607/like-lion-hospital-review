package com.example.likelionhospitalreivew.domain.dto;

import com.example.likelionhospitalreivew.domain.User;
import com.example.likelionhospitalreivew.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinRequest {
    private String userName;
    private String password;
    private UserRole userRole;

    public UserJoinRequest(String userName, String password) {
    }

    public static User toEntity(String userName, String password, UserRole userRole) {
        return User.builder()
                .userName(userName)
                .password(password)
                .userRole(userRole)
                .build();
    }
}
