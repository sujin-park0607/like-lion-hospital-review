package com.example.likelionhospitalreivew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikeLionHospitalReivewApplication {

	public static void main(String[] args) {
		SpringApplication.run(LikeLionHospitalReivewApplication.class, args);
	}

}
